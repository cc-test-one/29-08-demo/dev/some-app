package com.company.someproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class SomeProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SomeProjectApplication.class, args);
    }
}
