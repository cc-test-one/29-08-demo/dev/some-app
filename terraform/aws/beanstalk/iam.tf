resource "aws_iam_instance_profile" "ec2" {
  name_prefix = "${var.name}-eb-ec2-"
  role = aws_iam_role.ec2.name
}

resource "aws_iam_role" "ec2" {
  name_prefix = "${var.name}-eb-ec2-"
  assume_role_policy = data.aws_iam_policy_document.ec2.json
}
data "aws_iam_policy_document" "ec2" {
  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    effect = "Allow"
  }

  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ssm.amazonaws.com"]
    }

    effect = "Allow"
  }
}